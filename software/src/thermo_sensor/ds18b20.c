#ifndef THERMO_DS18B20
#define THERMO_DS18B20

#include <avr/io.h>
#include <util/delay.h>
#include <stdbool.h>

/*
Здесь необходимо указать, к какому порту подключен термодатчик 
И на какой ножке находится подключение
*/
#define THERMO_DDR DDRD
#define THERMO_PORT PORTD
#define THERMO_PIN PIND1
#define DS18B20 1

/*
Команды термодатчика, вынес основные для чтения и записи памяти
Время преобразования зависит от выбранного разрешения
Для подробностей см. даташиты DS18B20
Последовательность команд при работе с датчиком - сигнал сброса, сигнал ROM, команда и полезная нагрузка
*/
#define SKIP_ROM 0xCC
#define READ_ROM 0x33
#define SEARCH_ROM 0xF0
#define MATCH_ROM 0x55
#define CONVERT_T 0X44
#define WRITE_RAM 0x4E
#define READ_RAM 0xBE
#define COPY_RAM_TO_EEPROM 0x48
#define COPY_RAM_FROM_EEPROM 0xB8

/*
Команда последовательной отправки данных
command - байт данных, который необходимо отслать
Этим байтом может быть как команда, 
Так и полезная нагрузка (данные для ОЗУ или часть ROM-кода)
ВСЕ ДАННЫЕ ПЕРЕДАЮТСЯ МЛАДШИМ БИТОМ ВПЕРЁД!
*/
void sendSerialData(char command){
	for (uint8_t bit = 0; bit < 8; bit++) {
		//В начале работы линия проваливается, затем в зависимости от бита,
		//держится в низком состоянии либо до конца слота записи (0), либо 15 мкс (1)
		THERMO_DDR |= (1<<DS18B20);
		THERMO_PORT &=~(1<<DS18B20);
		if (command%2 != 0) {
			//запись 1 по таймингам
			_delay_us(14);
			THERMO_DDR &=~(1<<DS18B20);
			_delay_us(50);
		}
		else {
			//запись 0 по таймингам;
			_delay_us(59);
			THERMO_DDR &=~(1<<DS18B20);
		}
		command>>=1;
	}

}

/*
Сигнал сброса-присутствия
Он должен инициализировать любую операцию с датчиком
Если получен сигнал присутствия, то функция вернет true
В противном случае будет false
*/
bool resetPrecenseSignal() {
	THERMO_DDR |= (1<<DS18B20);
	THERMO_PORT &= ~(1<<DS18B20);
	_delay_us(480);
	THERMO_DDR &= ~(1<<DS18B20);
	_delay_us(60);
	for (char i = 0; i < 180; i++) {
		if(THERMO_PIN & ~(1<<DS18B20)){
			_delay_us(420);
			return true;	
		}
	}
	return false;
}
/*
Функция генерации слота чтения, используется в readByteOnWire
МК проваливает шину, затем отпускает её, переходя в режим слушателя
*/
void generateReadSlot() {
	THERMO_PORT &= ~(1<<DS18B20);
	THERMO_DDR |= (1<<DS18B20);
	THERMO_DDR &= ~(1<<DS18B20);
}
/*
Непосредственное чтение байта данных
Нужные байты см. в даташите (температура располагается в первых двух байтах памяти)
Функция производит генерацию слота чтения 8 раз подряд
После каждой генерации ждёт 11мкс (в даташите указано, что данные, выдаваемые термо, валидны на протяжении 15мкс от начала слота чтения)
Затем производит чтение из пина порта, на котором повешен термо с последующим наложением битовой маски
После чего устанавливает 1 в тех битах результата, которые соответствуют битам в ответе датчика
ВСЕ ДАННЫЕ ПЕРЕДАЮТСЯ МЛАДШИМ БИТОМ ВПЕРЁД!
*/
uint8_t readByteOnWire(){
	uint8_t resultByte = 0;
	for (uint8_t bit = 0; bit < 8; bit ++) {
		generateReadSlot();
		_delay_us(11);
		uint8_t data = THERMO_PIN;
		if (data & (1 << DS18B20))
			resultByte |= (1 << bit);
		_delay_us(49);
	}
	return resultByte;
}

#endif //THERMO_DS18B20